import {greet} from './greeter';

describe('AppComponent', () => {
    beforeEach(() => {
     console.log("This will be called before running each test case")
    });
  
    it('testing success case', () => {
      const result = greet("Manish", "Welcome");
      expect(result).toEqual('Welcome Manish');
    });

    it('negative test case', () => {
        const result = greet("Manish", "Hi");
        expect(result).not.toEqual('Welcome Manish');
      });
});
