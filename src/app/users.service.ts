import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './02-class/User';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUser(num1:number, num2:number){
    return new Promise((resolve, reject)=>{
      setTimeout((val1:number, val2:number)=>{
        //resolve(num1 + num2);
        reject("the service is temporarily down");
      },1,[num1,num2]);
    });
  }

  getAllUsers(flag:boolean):Observable<User[]>{
    if(flag){
      return this.http.get<User[]>('http://test.api.com');
    }else {
    return Observable.throw('Invalid parameters');
    }
  }

}
